﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingConsole2.Services
{
    class CalculatorService
    {
        public void mainMenu()
        {
            bool flag = false;
            do
            {
                Console.Clear();

                Console.Write("Type your First Number : ");
                decimal x1 = Int32.Parse(Console.ReadLine());
                Console.Write("Type your Second Number : ");
                decimal x2 = Int32.Parse(Console.ReadLine());
                Console.Write("Type Your Operator [+,-,*,/] : ");
                string op = Console.ReadLine();

                if (op == "+") Console.WriteLine(add(x1, x2));
                else if (op == "-") Console.WriteLine(substract(x1, x2));
                else if (op == "*") Console.WriteLine(multiply(x1, x2));
                else if (op == "/") Console.WriteLine(divison(x1, x2));
                else
                {
                    Console.WriteLine("Operator salah");
                    Console.ReadLine();
                    flag = true;
                }
            } while (flag);
        }

        public decimal add(decimal x1,decimal x2)
        {
            return x1 + x2;
        }

        public decimal substract(decimal x1, decimal x2)
        {
            return x1 - x2;
        }

        public decimal multiply(decimal x1, decimal x2)
        {
            return x1 * x2;
        }

        public decimal divison(decimal x1, decimal x2)
        {
            return x1 / x2;
        }
    }
}
