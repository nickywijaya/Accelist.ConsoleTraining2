﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.TrainingConsole2.Services
{
    class ReverseString
    {
        public void mainMenu()
        {
            Console.Clear();

            Console.Write("Masukan Huruf yang mau di Balik : ");
            string words = Console.ReadLine();

            //balikin jadi reverse harus diubah jadi char array dulu
            char[] reversedWords = words.ToCharArray();
            Array.Reverse(reversedWords);
            Console.Write("Huruf terbalik anda : " + new string(reversedWords));
        }
    }
}
