﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accelist.TrainingConsole2.Services;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Globalization;

namespace Accelist.TrainingConsole2
{
    class Program
    {
        //private readonly CalculateService calculateService;
        //Program(CalculateService calculateService)
        //{
        //    this.calculateService = calculateService;
        //}

        static void Main(string[] args)
        {
            #region Latihan
            int menuInput = 0;

            do   
            {
                Console.Clear();
                Console.Write(@"Menu :
1. Hello + Name
2. Calculator
3. Reverse String
Input : ");
                menuInput = Int32.Parse(Console.ReadLine());

                if (menuInput == 1)
                {
                    new HelloService().mainMenu();
                }
                else if(menuInput == 2)
                {
                    new CalculatorService().mainMenu();
                }
                else if (menuInput == 3)
                {
                    new ReverseString().mainMenu();
                }
                Console.ReadLine();
            } while (menuInput > 0 || menuInput < 4);
            #endregion


            #region Training
            //            string message = "";
            //            int myNumber = 0;

            //            int number = Int32.Parse(Console.ReadLine());
            //            number = number + 3;
            //            Console.WriteLine(number);

            //            List<string> names = new List<string>();
            //            names.Add("Hello");
            //            names.Add("World");

            //            Console.WriteLine(names[0]);


            //            foreach(var x in names)
            //            {
            //                Console.WriteLine(x);
            //            }

            //            using (var stream = new MemoryStream())
            //            {
            //                int member = 0;
            //                message = "haloo";
            //            }

            //            var money = 30000;
            //            string stringMoney = money.ToString("N2");
            //            Console.WriteLine(stringMoney);

            //            int z = Int32.Parse("50000");
            //            Console.WriteLine(z);

            //            //cek try parse
            //            int result = 0;//ini false
            //            bool value1 = Int32.TryParse("123", out result);
            //            Console.WriteLine(value1);

            //            //DateTime
            //            DateTime today = DateTime.Now;

            //            DateTime utc = DateTime.UtcNow;

            //            //coba string format
            //            decimal price = 17.2m;
            //            string s = String.Format("{0:C2}", price);
            //            Console.WriteLine(s);

            //            //mau ganti culturenya, tapi laen kali jgn currentCulture, msti di ganti jadi custom id-ID
            //            Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");
            //            var cultureInfo = Thread.CurrentThread.CurrentCulture;   
            //            var numberFormatInfo = (NumberFormatInfo)cultureInfo.NumberFormat.Clone();
            //            numberFormatInfo.CurrencySymbol = "Rp ";

            //            var prices = 60000;
            //            var formattedPrice = prices.ToString("C", numberFormatInfo);

            //            Console.WriteLine(formattedPrice);

            //            //mirip2 tag <pre>
            //            string pre = @"mantap
            //'sekali'
            //coy \
            //""bisa""
            //enter kyk
            //pre      haloo";

            //            //StringBuilder vs string
            //            StringBuilder sb = new StringBuilder();
            //            sb.Append("hi");
            //            sb.Append(" brooo mantap sekali");

            //            Console.WriteLine("\n\n" + sb);

            //            Console.WriteLine(pre);

            //            myNumber = 10;

            //            CalculateService.PriorMessage();
            //            Console.WriteLine(Application.MultiplyByMany(2, 3));
            //            Console.Read();
            #endregion


        }
    }

    public class Application
    {

        // /// ini buat kasih tau fungsi kita ngapain pas di hover
        /// <summary>
        /// for mutiplty input numbers
        /// </summary>
        /// <param name="args"></param>
        public static int MultiplyByMany(int x, int x2)
        {
            Console.WriteLine("Your first number : " + x);
            Console.WriteLine($"Your first number : {x2}");
            return x * x2;
        }

        public bool checkValue()
        {
            int input = 0;
            bool x;
            x = (input == 0) ?  false :  true;

            return x;
        }
    }
}
